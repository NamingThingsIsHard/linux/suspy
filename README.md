# suspy

Super User Sub Process pYthon - A subprocess that with superuser rights that allows the parent to execute only certain commands as root without having to request SU permissions for each command.

This is practically a super user daemon only for one parent process.

## How it works

The developer subclasses `Suspy` and overrides `get_parser()` to return an `argparse.ArgumentParser`
 and overrides `handle_args(args)` to call the necessary functions when arguments have been parsed.

A `Suspy` process can then be started (the best time to do that would be program start).
`Suspy` will then hang around as a subprocess that accepts string input through `stdin` using piping (`subprocess.PIPE`).

The input is then split by `shlex.split()` parsed by the `ArgumentParser` and `handle_args(args)` is called.
Output is printed to stdout and terminated with an EOF.
`Suspy` will then be ready to execute a new command.
