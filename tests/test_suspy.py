"""
suspy
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import unittest
from pathlib import Path

from suspy.suspy import Suspy

THIS = Path(__file__)
TESTS_PATH = THIS.parent
PROJECT_PATH = TESTS_PATH.parent


class GetModulePathTest(unittest.TestCase):
    def test_get_module_path(self):
        self.assertEqual(Suspy.get_module_path(), str(PROJECT_PATH / "suspy" / "suspy.py"))


class BuildStdMessageTest(unittest.TestCase):
    def test_no_args(self):
        self.assertEqual(Suspy.build_stdin_message("command"), "command")

    def test_args(self):
        self.assertEqual(Suspy.build_stdin_message("command", "arg1", "arg2"), "command arg1 arg2")

    def test_kwargs(self):
        self.assertEqual(
            Suspy.build_stdin_message("command", arg1="value1", arg2="value2"),
            "command --arg1 value1 --arg2 value2",
        )

    def test_args_and_kwargs(self):
        self.assertEqual(
            Suspy.build_stdin_message("command", "arg1", "arg2", kwarg1="value1", kwarg2="value2"),
            "command --kwarg1 value1 --kwarg2 value2 arg1 arg2",
        )

    def test_quotable_args(self):
        self.assertEqual(
            Suspy.build_stdin_message("command", "this is an arg", "how about a second arg"),
            "command 'this is an arg' 'how about a second arg'")

    def test_quotable_kwarg_vals(self):
        self.assertEqual(
            Suspy.build_stdin_message("command", kwarg1='"quote" this', kwarg2="and this too"),
            "command --kwarg1 '\"quote\" this' --kwarg2 'and this too'")
