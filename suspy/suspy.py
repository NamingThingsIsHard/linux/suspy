#!/usr/bin/env python3
"""
suspy
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import importlib.util
import os
import shlex
import subprocess
from argparse import ArgumentParser, Namespace
from typing import Optional


class Suspy:
    """
    Super User Sub Process (SUSP) in Python

    Subclass this in a separate file and override the overridable methods.
    The file MUST be executable (thus have a __main__) that runs .main().

    See the bottom of this file for an example
    """

    def __init__(self):
        self.proc: Optional[subprocess.Popen] = None

    @classmethod
    def main(cls):
        """Starts a main loop to wait for commands from stdin"""
        # TODO: implement main loop
        pass

    @classmethod
    def get_module_path(cls):
        """Find the path to where this class's module is"""
        path = importlib.util.find_spec(cls.__module__)
        return path.origin

    @classmethod
    def get_parser(cls) -> ArgumentParser:
        """Create an argument parser here that will be used to call the commands passed in stdin"""
        raise NotImplementedError()

    @classmethod
    def handle_args(cls, args: Namespace):
        """Handle the arguments parsed by the ArgumentParser created in get_parser"""
        raise NotImplementedError()

    @staticmethod
    def build_stdin_message(command: str, *args, **kwargs):
        """Prepares the arguments to be sent to the SUSP over stdin"""
        items = [command]
        for kwarg, kwvalue in kwargs.items():
            items.extend([f"--{kwarg}", shlex.quote(kwvalue)])
        items.extend([shlex.quote(arg) for arg in args])
        return " ".join(items)

    def __enter__(self):
        """Creates a context and starts a SUSP"""
        # TODO: implement

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Kill the context and the running SUSP"""
        # TODO: implement

    def start(self) -> 'Suspy':
        """Start a SUSP """
        run_args_list = []
        module_command = [self.get_module_path()]
        if "DISPLAY" in os.environ:
            run_args_list.append(["pkexec"] + module_command)
            run_args_list.append(["gksudo"] + module_command)
            run_args_list.append(["kdesudo", "-c"] + module_command)

        run_args_list.append(["sudo"] + module_command)

        for run_args in run_args_list:
            try:
                self.proc = subprocess.Popen(
                    run_args,
                    stdin=subprocess.PIPE,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                )
                return self
            except FileNotFoundError:
                # Couldn't find the file to execute or it isn't executable
                continue
        raise Exception("Couldn't start as root")

    def execute(self, command: str, *args, **kwargs):
        """Executes a command as a root user using the running susp"""
        # The process should exist
        if not self.proc:
            raise Exception("Process isn't running")
        # Process should still be running
        self.proc.poll()
        if self.proc.returncode is not None:
            # Add stderr to exception?
            raise Exception("Process isn't running anymore")

        self.proc.stdin.write(
            self.build_stdin_message(command, *args, **kwargs) + "\n"
        )  # needs newline to terminate line for read()


if __name__ == "__main__":
    Suspy.main()
